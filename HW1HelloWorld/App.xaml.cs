﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HW1HelloWorld
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HW1HelloWorldPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
          
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");

        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");

        }
    }
}
